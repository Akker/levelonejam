Event	ID	Name			Wwise Object Path	Notes
	79087081	ResumeTruckAmbience			\Game\Ambience\ResumeTruckAmbience	
	106077997	Play_BillboardStart			\Game\Events\Play_BillboardStart	
	107826350	PauseRadio			\Game\Radio\PauseRadio	
	164866881	EngineStart			\Game\Feedback\EngineStart	
	319935972	Play_DumpSiteEnd			\Game\Events\Play_DumpSiteEnd	
	394434668	Play_DumpSite01			\Game\Events\Play_DumpSite01	
	394434670	Play_DumpSite03			\Game\Events\Play_DumpSite03	
	394434671	Play_DumpSite02			\Game\Events\Play_DumpSite02	
	443547655	AutoShopend			\Game\Events\AutoShopend	
	625733901	Play_GasStationStart			\Game\Events\Play_GasStationStart	
	794605376	PlayGasStation02			\Game\Events\GasStation\PlayGasStation02	
	794605377	PlayGasStation03			\Game\Events\GasStation\PlayGasStation03	
	794605379	PlayGasStation01			\Game\Events\GasStation\PlayGasStation01	
	804698715	Play_TruckParkingStart			\Game\Events\Play_TruckParkingStart	
	970428702	Play_BrothelStart			\Game\Events\Play_BrothelStart	
	996029715	ResumeTruckerFeedbackLoop			\Game\Feedback\ResumeTruckerFeedbackLoop	
	1029295979	Play_RestStopEnd			\Game\Events\Play_RestStopEnd	
	1084356852	Play_Billboard03			\Game\Events\Play_Billboard03	
	1084356853	Play_Billboard02			\Game\Events\Play_Billboard02	
	1084356854	Play_Billboard01			\Game\Events\Play_Billboard01	
	1145206700	VegetableStand2			\Game\Events\VegetableStand\VegetableStand2	
	1145206701	VegetableStand3			\Game\Events\VegetableStand\VegetableStand3	
	1145206703	VegetableStand1			\Game\Events\VegetableStand\VegetableStand1	
	1212036097	AutoShop01			\Game\Events\AutoShop01	
	1212036098	AutoShop02			\Game\Events\AutoShop02	
	1212036099	AutoShop03			\Game\Events\AutoShop03	
	1269015798	StopRadio			\Game\Radio\StopRadio	
	1288594709	Play_AutoShopStart			\Game\Events\Play_AutoShopStart	
	1364393352	PauseTruckerFeedbackLoop			\Game\Feedback\PauseTruckerFeedbackLoop	
	1381963860	Play_GasStation03			\Game\Events\Play_GasStation03	
	1381963861	Play_GasStation02			\Game\Events\Play_GasStation02	
	1381963862	Play_GasStation01			\Game\Events\Play_GasStation01	
	1382336663	Play_DumpSiteStart			\Game\Events\Play_DumpSiteStart	
	1415972064	Play_LiquorStore01			\Game\Events\Play_LiquorStore01	
	1415972066	Play_LiquorStore03			\Game\Events\Play_LiquorStore03	
	1415972067	Play_LiquorStore02			\Game\Events\Play_LiquorStore02	
	1429535606	PlayTruckerFeedbackLoop			\Game\Feedback\PlayTruckerFeedbackLoop	
	1436376325	ResumeRadio			\Game\Radio\ResumeRadio	
	1454516304	Play_RestStopStart			\Game\Events\Play_RestStopStart	
	1472800052	Play_FastFoodStart			\Game\Events\Play_FastFoodStart	
	1541792432	PlayMexican02			\Game\Events\Mexican\PlayMexican02	
	1541792433	PlayMexican03			\Game\Events\Mexican\PlayMexican03	
	1541792435	PlayMexican01			\Game\Events\Mexican\PlayMexican01	
	1750266586	PlayTruckAmbienceLoop			\Game\Ambience\PlayTruckAmbienceLoop	
	1750448532	Play_SpeedCamStart			\Game\Events\Play_SpeedCamStart	
	1775693328	Play_MexicanEnd			\Game\Events\Play_MexicanEnd	
	1856143971	Play_BikerStart			\Game\Events\Play_BikerStart	
	1869456000	Play_TruckParkingEnd			\Game\Events\Play_TruckParkingEnd	
	2002511053	PlayClick			\Game\UI\PlayClick	
	2079244539	EngineStop			\Game\Feedback\EngineStop	
	2122396316	Play_VeggieStand03			\Game\Events\Play_VeggieStand03	
	2122396317	Play_VeggieStand02			\Game\Events\Play_VeggieStand02	
	2122396318	Play_VeggieStand01			\Game\Events\Play_VeggieStand01	
	2233950790	Play_BillboardEnd			\Game\Events\Play_BillboardEnd	
	2281615882	Play_TequilaEnd			\Game\Events\Play_TequilaEnd	
	2349312171	Play_MexicanStart			\Game\Events\Play_MexicanStart	
	2460650122	PauseTruckAmbience			\Game\Ambience\PauseTruckAmbience	
	2485707093	Play_BrothelEnd			\Game\Events\Play_BrothelEnd	
	2590417944	Play_LiquorStoreEnd			\Game\Events\Play_LiquorStoreEnd	
	2676681465	Play_TequilaStart			\Game\Events\Play_TequilaStart	
	2851737995	Play_ShoppingMallEnd			\Game\Events\Play_ShoppingMallEnd	
	2855747495	Play_SpeedCamEnd			\Game\Events\Play_SpeedCamEnd	
	2896398328	Play_Tequila03			\Game\Events\Play_Tequila03	
	2896398329	Play_Tequila02			\Game\Events\Play_Tequila02	
	2896398330	Play_Tequila01			\Game\Events\Play_Tequila01	
	3026204914	CarPassing			\Game\Ambience\CarPassing	
	3089599978	Accelerate			\Game\Feedback\Accelerate	
	3123566937	Play_MagicFireStart			\Game\Events\Play_MagicFireStart	
	3126943301	PlayHitchhiker01			\Game\Events\Hitchhiker\PlayHitchhiker01	
	3126943302	PlayHitchhiker02			\Game\Events\Hitchhiker\PlayHitchhiker02	
	3126943303	PlayHitchhiker03			\Game\Events\Hitchhiker\PlayHitchhiker03	
	3127708004	Play_Brothel02			\Game\Events\Play_Brothel02	
	3127708005	Play_Brothel03			\Game\Events\Play_Brothel03	
	3127708007	Play_Brothel01			\Game\Events\Play_Brothel01	
	3130371464	Play_TruckParking01			\Game\Events\Play_TruckParking01	
	3130371466	Play_TruckParking03			\Game\Events\Play_TruckParking03	
	3130371467	Play_TruckParking02			\Game\Events\Play_TruckParking02	
	3214869955	Play_LiquorStoreStart			\Game\Events\Play_LiquorStoreStart	
	3232272455	Play_FastFoodEnd			\Game\Events\Play_FastFoodEnd	
	3241612321	Play_SpeedCam01			\Game\Events\Play_SpeedCam01	
	3241612322	Play_SpeedCam02			\Game\Events\Play_SpeedCam02	
	3241612323	Play_SpeedCam03			\Game\Events\Play_SpeedCam03	
	3400226160	Play_ShoppingMallStart			\Game\Events\Play_ShoppingMallStart	
	3417114433	Play_FastFood01			\Game\Events\Play_FastFood01	
	3417114434	Play_FastFood02			\Game\Events\Play_FastFood02	
	3417114435	Play_FastFood03			\Game\Events\Play_FastFood03	
	3470630149	Play_VeggieStandStart			\Game\Events\Play_VeggieStandStart	
	3492074090	Play_MagicFireEnd			\Game\Events\Play_MagicFireEnd	
	3504740484	Brothel1			\Game\Events\Brothel\Brothel1	
	3504740486	Brothel3			\Game\Events\Brothel\Brothel3	
	3504740487	Brothel2			\Game\Events\Brothel\Brothel2	
	3521206566	Play_GasStationEnd			\Game\Events\Play_GasStationEnd	
	3528264838	PlayRadioLoop			\Game\Radio\PlayRadioLoop	
	3529574258	TruckerMood4			\Game\Feedback\TruckerStatusSwitch\TruckerMood4	
	3529574259	TruckerMood5			\Game\Feedback\TruckerStatusSwitch\TruckerMood5	
	3529574260	TruckerMood2			\Game\Feedback\TruckerStatusSwitch\TruckerMood2	
	3529574261	TruckerMood3			\Game\Feedback\TruckerStatusSwitch\TruckerMood3	
	3529574263	TruckerMood1			\Game\Feedback\TruckerStatusSwitch\TruckerMood1	
	3623471800	Play_BikerEnd			\Game\Events\Play_BikerEnd	
	3785305304	Play_MagicFire03			\Game\Events\Play_MagicFire03	
	3785305305	Play_MagicFire02			\Game\Events\Play_MagicFire02	
	3785305306	Play_MagicFire01			\Game\Events\Play_MagicFire01	
	3817841646	Play_VeggieStandEnd			\Game\Events\Play_VeggieStandEnd	
	3819494096	Play_Hitchhiker01			\Game\Events\Play_Hitchhiker01	
	3819494098	Play_Hitchhiker03			\Game\Events\Play_Hitchhiker03	
	3819494099	Play_Hitchhiker02			\Game\Events\Play_Hitchhiker02	
	3936872851	Play_HitchhikerStart			\Game\Events\Play_HitchhikerStart	
	4031716456	Play_HitchhikerEnd			\Game\Events\Play_HitchhikerEnd	
	4073760789	Play_ShoppingMall01			\Game\Events\Play_ShoppingMall01	
	4073760790	Play_ShoppingMall02			\Game\Events\Play_ShoppingMall02	
	4073760791	Play_ShoppingMall03			\Game\Events\Play_ShoppingMall03	
	4113547392	Play_Biker01			\Game\Events\Play_Biker01	
	4113547394	Play_Biker03			\Game\Events\Play_Biker03	
	4113547395	Play_Biker02			\Game\Events\Play_Biker02	
	4216331358	CarCrash			\Game\Feedback\CarCrash	
	4219724149	Play_RestStop01			\Game\Events\Play_RestStop01	
	4219724150	Play_RestStop02			\Game\Events\Play_RestStop02	
	4219724151	Play_RestStop03			\Game\Events\Play_RestStop03	

Effect plug-ins	ID	Name	Type				Notes
	1293178626	In_My_Car	Wwise Parametric EQ			

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	16138801	SpeedCamEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\SpeedCamEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\SpeedCamEnd		30037
	20852335	Billboard01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Billboard01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Billboard01		42812
	24352151	MexicanEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\MexicanEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\MexicanEnd		41991
	72922237	GasStation02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\GasStation02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\GasStation\GasStation02		23370
	75307078	DumpSite02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\DumpSite02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\DumpSite02		28255
	82477360	Brothel03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Brothel03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Brothel03		22015
	88418700	TequilaEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\TequilaEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\TequilaEnd		70132
	89987410	BikerEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\BikerEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\BikerEnd		38054
	97538178	Inbetweens02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Inbetweens02_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\Inbetweens02\Inbetweens02		170783
	131684608	VeggieStand02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\VeggieStand02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\VeggieStand02		27520
	132819885	LiquorStore03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\LiquorStore03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\LiquorStore03		48552
	173822471	TruckParkingStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\TruckParkingStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\TruckParkingStart		69014
	186742741	TruckParkingEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\TruckParkingEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\TruckParkingEnd		34389
	196307747	LiquorStore01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\LiquorStore01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\LiquorStore01		24359
	205931460	FastFood01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\FastFood01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\FastFood01		15415
	224942440	CarCrash2	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\CarCrash2_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Feedback\CarCrash\CarCrash2		27452
	226042159	HitchhikerStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\HitchhikerStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\HitchhikerStart		26694
	247459995	TruckParking01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\TruckParking01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\TruckParking01		32089
	255264045	LiquorStore02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\LiquorStore02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\LiquorStore02		42416
	255736464	Inbetweens06	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Inbetweens06_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\Inbetweens06\Inbetweens06		121888
	273881282	AutoShopStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\AutoShopStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\AutoShopStart		21624
	277095693	TruckParking02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\TruckParking02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\TruckParking02		28013
	290359375	DumpSiteStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\DumpSiteStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\DumpSiteStart		59340
	304540496	SpeedCam03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\SpeedCam03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\SpeedCam03		52870
	307871516	ShoppingMallStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\ShoppingMallStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\ShoppingMallStart		30893
	310146590	BillboardStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\BillboardStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\BillboardStart		37197
	312097807	GasStationStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\GasStationStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\GasStationStart		29266
	319588309	MexicanStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\MexicanStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\MexicanStart		18972
	330300471	TruckParking03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\TruckParking03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\TruckParking03		27000
	331669350	HitchhikerEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\HitchhikerEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\HitchhikerEnd		37718
	345134348	Billboard02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Billboard02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Billboard02		69236
	362093991	FastFood02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\FastFood02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\FastFood02		16019
	365579220	GasStation02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\GasStation02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\GasStation02		23370
	370177120	Tequila02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Tequila02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Tequila02		45683
	381550490	IntroA01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\IntroA01_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\IntroA01\IntroA01		145457
	384354541	RestStopEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\RestStopEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\RestStopEnd		32057
	390180407	Tequila01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Tequila01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Tequila01		21369
	396147477	Inbetweens01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Inbetweens01_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\Inbetweens01\Inbetweens01		620873
	397684633	ShoppingMall03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\ShoppingMall03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\ShoppingMall03		39532
	403401896	AutoShop03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\AutoShop03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\AutoShop03		23242
	415544274	IntroA02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\IntroA02_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\IntroA02\IntroA02		107343
	417862479	LiquorStoreStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\LiquorStoreStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\LiquorStoreStart		27047
	418065535	Biker02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Biker02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Biker02		21799
	431939847	BikerStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\BikerStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\BikerStart		29014
	432513518	Vamos a cocinar con mi amigos	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Vamos a cocinar con mi amigos_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\Vamos a cocinar con mi amigos\Vamos a cocinar con mi amigos		2507196
	433117578	ShoppingMallEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\ShoppingMallEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\ShoppingMallEnd		34123
	444441539	Red Haired Blond	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Red Haired Blond_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\Red Haired Blond\Red Haired Blond		3332306
	479399837	ShoppingMall02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\ShoppingMall02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\ShoppingMall02		63669
	507144052	Billboard03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Billboard03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Billboard03		77408
	520847716	Inbetweens07	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Inbetweens07_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\Inbetweens07\Inbetweens07		152937
	523349530	RestStop01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\RestStop01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\RestStop01		22134
	562968147	FastFoodEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\FastFoodEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\FastFoodEnd		39145
	583074910	Hitchhiker03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Hitchhiker03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Hitchhiker03		13372
	589458689	GasStation01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\GasStation01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\GasStation\GasStation01		27951
	592022704	Mexican03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Mexican03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\Mexicans\Mexican03		16984
	594904820	CarCrash4	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\CarCrash4_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Feedback\CarCrash\CarCrash4		16102
	597062195	MagicFire03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\MagicFire03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\MagicFire03		85678
	599381679	MagicFire01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\MagicFire01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\MagicFire01		64517
	615373923	DumpSite01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\DumpSite01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\DumpSite01		37711
	640427669	Mexican02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Mexican02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\Mexicans\Mexican02		22050
	642480184	FastFoodStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\FastFoodStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\FastFoodStart		43281
	680218382	GasStation03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\GasStation03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\GasStation\GasStation03		18705
	680647182	Hitchhiker02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Hitchhiker02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\Hitchhiker\Hitchhiker02		44697
	684240385	Brothel02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Brothel02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Brothel02		15961
	697863633	SpeedCamStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\SpeedCamStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\SpeedCamStart		22762
	704765245	EngineLoop	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\EngineLoop_CB1F3167.wem		\Actor-Mixer Hierarchy\Default Work Unit\Ambience\EngineLoop		438072
	709799906	IntroB01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\IntroB01_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\IntroB01\IntroB01		138295
	737215534	TequilaStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\TequilaStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\TequilaStart		56945
	737701201	AutoShop02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\AutoShop02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\AutoShop02		37097
	746139692	Inbetweens03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Inbetweens03_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\Inbetweens03\Inbetweens03		115825
	762619546	Swamp Blues	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Swamp Blues_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\Swamp Blues\Swamp Blues		5632852
	768178755	AutoShopend	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\AutoShopend_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\AutoShopend		26819
	768969636	FastFood03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\FastFood03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\FastFood03		25750
	771490313	Hitchhiker02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Hitchhiker02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Hitchhiker02		44697
	776713990	RestStop02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\RestStop02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\RestStop02		49112
	784107540	RestStopStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\RestStopStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\RestStopStart		51111
	788317918	Biker03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Biker03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Biker03		16739
	790877221	Brothel01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Brothel01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Brothel01		24025
	794637222	RestStop03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\RestStop03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\RestStop03		96481
	801674807	Inbetweens05	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Inbetweens05_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\Inbetweens05\Inbetweens05		214069
	802842185	BrothelStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\BrothelStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\BrothelStart		21957
	811904577	MagicFireEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\MagicFireEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\MagicFireEnd		52680
	820585760	IntroB02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\IntroB02_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\IntroB02\IntroB02		133006
	823310496	MagicFire02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\MagicFire02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\MagicFire02		95451
	824092076	SpeedCam02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\SpeedCam02_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\SpeedCam02		37108
	831057730	GasStation01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\GasStation01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\GasStation01		27951
	844846722	VeggieStandEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\VeggieStandEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\VeggieStandEnd		40343
	846394405	GasStation03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\GasStation03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\GasStation03		18705
	852155845	Inbetweens08	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Inbetweens08_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\Inbetweens08\Inbetweens08		199069
	861551444	DumpSiteEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\DumpSiteEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\DumpSiteEnd		34326
	878700038	CarCrash1	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\CarCrash1_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Feedback\CarCrash\CarCrash1		20523
	895160955	VeggieStand01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\VeggieStand01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\VeggieStand01		34977
	897398396	BrothelEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\BrothelEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\BrothelEnd		27997
	919433233	VeggieStandStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\VeggieStandStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\VeggieStandStart		26909
	925653998	BillboardEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\BillboardEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\BillboardEnd		39881
	931129896	IntroC02	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\IntroC02_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\IntroC02\IntroC02		104376
	932224202	EngineStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\EngineStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Ambience\EngineStart		204255
	936321797	MagicFireStart	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\MagicFireStart_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\MagicFireStart		46255
	939036644	IntroC01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\IntroC01_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\IntroC01\IntroC01		82853
	939810831	Click	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Click_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\UI\Click		7068
	942050069	Hitchhiker01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Hitchhiker01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\Hitchhiker\Hitchhiker01		23439
	949998685	Mexican01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Mexican01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\Mexicans\Mexican01		33402
	961660742	Biker01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Biker01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Biker01		16337
	963000894	DumpSite03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\DumpSite03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\DumpSite03		40782
	973286483	k 6	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\k 6_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Placeholder\k 6		5904
	974754028	AutoShop01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\AutoShop01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\AutoShop01		48255
	978511912	SpeedCam01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\SpeedCam01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\SpeedCam01		36810
	980674646	CarCrash3	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\CarCrash3_096A6829.wem		\Actor-Mixer Hierarchy\Default Work Unit\Feedback\CarCrash\CarCrash3		19805
	990419296	GasStationEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\GasStationEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\GasStationEnd		26399
	1005621908	Hitchhiker03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Hitchhiker03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\Hitchhiker\Hitchhiker03		13372
	1005902236	Tequila03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Tequila03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Tequila03		20884
	1006478173	ShoppingMall01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\ShoppingMall01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\ShoppingMall01		42593
	1046111050	VeggieStand03	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\VeggieStand03_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\VeggieStand03		22479
	1048097281	Inbetweens04	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Inbetweens04_CB1F3167.wem		\Interactive Music Hierarchy\Default Work Unit\Radio\Inbetweens04\Inbetweens04		182905
	1053872144	LiquorStoreEnd	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\LiquorStoreEnd_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\LiquorStoreEnd		5099
	1054551310	Hitchhiker01	Y:\Documents\GitHub\levelonejam\levelonejam_WwiseProject\.cache\Windows\SFX\Bounces\Hitchhiker01_A1793BE9.wem		\Actor-Mixer Hierarchy\Default Work Unit\Dialog\MIX\Bounces\Hitchhiker01		23439

