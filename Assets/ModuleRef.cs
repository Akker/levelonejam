﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleRef : MonoBehaviour
{
    public GameObject SpawnPoint;
    private List<Transform> MovingItems = new List<Transform>();

    Transform[] children;
    Vector3[] positions;
    Quaternion[] rotation;
    BoxCollider[][] boxes;

    public GameObject GetSpawnPoint()
    {
        ResetPosition();
        return SpawnPoint;
    }

    void Start()
    {
        children = new Transform[ transform.childCount];
        boxes = new BoxCollider[children.Length][];
        positions = new Vector3[children.Length];
        rotation = new Quaternion[children.Length];
        for (int i = 0; i < children.Length; i++)
        {
            children[i] = transform.GetChild(i);
            positions[i] = children[i].localPosition;
            rotation[i] = children[i].rotation;
            boxes[i] = children[i].GetComponentsInChildren<BoxCollider>();
        }
        /*
        foreach (Transform t in children)
        {
            if( t.gameObject.tag == "Car" || t.gameObject.tag == "Obstacle" )
                MovingItems.Add(t);
        }
        */
    }

    void Update() {
        
    }

    public void ResetPosition()
    {
        //Transform[] children = gameObject.GetComponentsInChildren<Transform>();
        //int i = 0;
        for (int i = 0; i < children.Length; i++)
        {
            children[i].localPosition = positions[i];
            children[i].rotation = rotation[i];
            Rigidbody temp = children[i].GetComponent<Rigidbody>();
            if (temp != null)
            {
                temp.velocity = Vector3.zero;
                for (int j = 0; j < boxes[i].Length; j++)
                {
                    boxes[i][j].enabled = true;
                }
            }
        }
        /*
        foreach (Transform t in children)
        {
            if( t.gameObject.tag == "Car" || t.gameObject.tag == "Obstacle" )
            {
                t.gameObject.transform.SetPositionAndRotation(MovingItems[i].position, MovingItems[i].rotation);
                i++;
            }
        }
        */
    }
}
