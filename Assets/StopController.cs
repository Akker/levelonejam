﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopController : MonoBehaviour
{
    public ThingSet Player;
    public GameObject RestartPoint;
    public GameObject StopPoint;
    public float Speed = 1f;

    float xStart;
    Vector3 startPosition;

    // Update loop events
    public GameEvent PauseGame;
    public GameEvent StartGame;
    
    // Decisions
    public GameEvent ShowDecisionUI;

    // Time management
    private bool stopFlag;
    private bool restartFlag;
    private float currentLerpTime;
    private float percentileTime;
    // Start is called before the first frame update
    void Start()
    {
        Vector3 newPosition = StopPoint.transform.position;
        newPosition.y = Player.GetItemsAtIndex(0).transform.position.y;
        StopPoint.transform.position = newPosition;

        newPosition = RestartPoint.transform.position;
        newPosition.y = Player.GetItemsAtIndex(0).transform.position.y;
        RestartPoint.transform.position = newPosition;
    }

    // Update is called once per frame
    void Update()
    {
        //increment timer once per frame
        currentLerpTime += Time.deltaTime;
        percentileTime = currentLerpTime / Speed; 
        if( stopFlag )
        {
            //Debug.Log("Moving to stop");
            //Debug.Log("Percentile time" + percentileTime);
            //Debug.Log("Percentile time" + percentileTime);
            Vector3 newPosition = Vector3.Lerp(startPosition, StopPoint.transform.position, percentileTime);
            //Vector3 newPosition = Player.Items[0].transform.position;
            //newPosition.x = (xStart / StopPoint.transform.position.x) * percentileTime;

            //Debug.Log(newPosition + " from: " + Player.Items[0].transform.position + " and: " + StopPoint.transform.position);
            Player.Items[0].transform.position = newPosition;
            if( Vector3.Distance(Player.Items[0].transform.position, StopPoint.transform.position) < 1f )
            {
                //Debug.Log("Distance reached");
                // Stop finished
                stopFlag = false;
                // Take decision
                ShowDecisionUI.Raise();
            }
        }
        
        if( restartFlag )
        {

            Player.Items[0].transform.position = Vector3.Lerp(startPosition, RestartPoint.transform.position, percentileTime);
            if( Vector3.Distance(Player.Items[0].transform.position, RestartPoint.transform.position) < 1f )
            {
                // Restart driving
                restartFlag = false;
                StartGame.Raise();       
            }
        }
    }

    public void Stop()
    {
        //Debug.Log("Stopping");
        currentLerpTime = 0f;
        stopFlag = true;
        PauseGame.Raise();
        xStart = Player.GetItemsAtIndex(0).transform.position.x;
        startPosition = Player.GetItemsAtIndex(0).transform.position;
        // AkSoundEngine.PostEvent("PauseRadio", gameObject);
        // AkSoundEngine.PostEvent("PauseTruckerFeedbackLoop", gameObject);
    }

    public void Restart()
    {

        //Debug.Log("Restarting");
        currentLerpTime = 0f;
        restartFlag = true;
        startPosition = Player.GetItemsAtIndex(0).transform.position;

        // AkSoundEngine.PostEvent("ResumeRadio", gameObject);
        // AkSoundEngine.PostEvent("ResumeTruckerFeedbackLoop", gameObject);
    }
}
