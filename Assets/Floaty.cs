﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floaty : MonoBehaviour
{

    Vector3 orgPosition;
    Vector3 newPosition;
    Vector3 lastPosition;
    public float maxMagnitude = 10;
    public float minMagnitude = 30;
    public float minTime = 1.0f;
    public float maxTime = 2.5f;
    public float minDistance = 30;
    public float maxDistance = 40;

    float currentTime;
    float timeToReach;

    private void Start()
    {
        orgPosition = transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime > timeToReach)
        {
            currentTime = 0;
            FindNewPosition();
        }
        transform.position = Vector3.Lerp(lastPosition, newPosition, currentTime / timeToReach);
    }

    void FindNewPosition()
    {
        lastPosition = transform.position;
        float distance = 0;
        int timesRun = 0;
        while(distance > maxDistance || distance < minDistance)
        {
            newPosition = new Vector2(Random.Range(-1.1f, 1.1f), Random.Range(-1.1f, 1.1f)).normalized;
            newPosition = (newPosition * Random.Range(minMagnitude, maxMagnitude)) + orgPosition;
            distance = Vector2.Distance(lastPosition, newPosition);
            timesRun++;
            if (timesRun > 20)
            {
                break;
            }
            //Debug.Log(distance);
        }

        timeToReach = Random.Range(minTime, maxTime);
    }
}
