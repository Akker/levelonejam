﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorShifter : ColorAlphaChanger
{
    // Start is called before the first frame update
    void Start()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0f);        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate() 
    {
        ShiftColor();
    }

    public override void ShiftColor()
    {
        if( shift )
        {
            image.color = Color.Lerp( image.color, nextColor, Time.deltaTime);
        }
    }
}
