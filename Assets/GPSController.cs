﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPSController : MonoBehaviour
{
    public BooleanVariable StopDecision;
    private bool UpcomingDecision = false;
    // Start is called before the first frame update
    void Start()
    {
        StopDecision.SetValue(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            SetStop();
        }
    }

    public void SetStop() 
    {
        Debug.Log("SetStop");
        if( UpcomingDecision )
        {
            Debug.Log("Decided to Stop");
            StopDecision.SetValue(true);
            UpcomingDecision = false;
        }
    }

    public void SetUpcoming()
    {
        Debug.Log("SetUpcoming");
        UpcomingDecision = true;
    }




}
