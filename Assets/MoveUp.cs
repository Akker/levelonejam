﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUp : MonoBehaviour
{
    public float speed;

    // Update is called once per frame
    void Update()
    {
        Vector3 newPosition = transform.position;
        newPosition += Vector3.up * speed * Time.deltaTime;
        transform.position = newPosition;
    }
}
