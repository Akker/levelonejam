﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishTutorial : MonoBehaviour
{
    public GameEvent ResumeUpdateLoop;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if( Input.GetKeyUp(KeyCode.Space) )
        {
            ResumeUpdateLoop.Raise();
            gameObject.SetActive(false);
        }   
    }
}
