﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BettySwinger : OnUpdate
{
    public float magnitude;
    public int delay;

    List<Vector3> delayPositions;
    List<float> delayFloat;

    private void Start()
    {
        delayPositions = new List<Vector3>();
        delayFloat = new List<float>();
        Vector3 tempStartPositions = calculateNewPosition();
        for (int i = 0; i < delay; i++)
        {
            delayPositions.Add(tempStartPositions);
            delayFloat.Add(0.0f);
        }
    }
    Vector3 calculateNewPosition()
    {
        float angle = Mathf.Rad2Deg * (Input.GetAxis("Horizontal") / 2);
        Quaternion newHeading = Quaternion.AngleAxis(-angle, Vector3.forward);
        transform.up = Vector3.up;
        transform.rotation = newHeading;

        Vector3 newPosition = transform.parent.position - (transform.up * magnitude);
        return newPosition;
    }

    public override void UpdateEventRaised(float deltaTime)
    {
        base.UpdateEventRaised(deltaTime);
        //delayPositions.Add(calculateNewPosition());
        delayFloat.Add((Input.GetAxis("Horizontal") / 2));
        //transform.position = delayPositions[0];
        //delayPositions.RemoveAt(0);
        transform.position = calculateNewPositionFromFloat(delayFloat[0]);
        delayFloat.RemoveAt(0);
    }
    Vector3 calculateNewPositionFromFloat(float value)
    {
        float angle = Mathf.Rad2Deg * value;
        Quaternion newHeading = Quaternion.AngleAxis(-angle, Vector3.forward);
        transform.up = Vector3.up;
        transform.rotation = newHeading;

        Vector3 newPosition = transform.parent.position - (transform.up * magnitude);
        return newPosition;
    }
}
