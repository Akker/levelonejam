﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LoadSceneSetOnAwake : MonoBehaviour {
	public SceneSetVariable scenesToLoad;
	public LoadSceneSetFromEvent sceneLoader;
	// Use this for initialization
	void Start () {
		sceneLoader.scene = scenesToLoad.Value;
		sceneLoader.LoadSceneSet();
	}
	
}
