﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffroadChecker : OnUpdate
{
    public float timeBeforeDamage = 3.0f;
    public IntVariable trucks;

    float currentTime;
    bool isOffRoad;
    public override void UpdateEventRaised(float deltaTime)
    {
        if (isOffRoad)
        {
            currentTime += deltaTime;
            if (currentTime > timeBeforeDamage)
            {
                DealDamager();
                currentTime = 0;
            }
        }
    }
    void DealDamager()
    {
        trucks.ApplyChange(-1);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("OffRoad"))
        {
            isOffRoad = true;

        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("OffRoad"))
        {
            isOffRoad = false;
            currentTime = 0;

        }
    }
}
