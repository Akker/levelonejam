﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCollisionSystem : MonoBehaviour
{
    public GameObjectVariable CollidedObject;
    public IntVariable truck;
    public IntVariable fucks;
    

    public void Collision()
    {
        if(CollidedObject.Value.layer == LayerMask.NameToLayer("Car"))
        {
            HitCar();
        } else if (CollidedObject.Value.layer == LayerMask.NameToLayer("Obstacle"))
        {
            HitOther();
        }
        //Debug.Log(CollidedObject.Value.layer + " tried: " + LayerMask.NameToLayer("Obstacle"));
    }

    void HitCar()
    {
        //Debug.Log("Hit car");
        //AkSoundEngine.PostEvent("CarCrash", gameObject);
        truck.ApplyChange(-2);
        fucks.ApplyChange(10);
    }
    void HitOther()
    {
        truck.ApplyChange(-1);
        fucks.ApplyChange(5);
    }
}
