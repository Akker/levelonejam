﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultyController : OnUpdate
{
    public FloatVariable difficulty;
    public float startingDifficulty = 1;
    public float increasePercent = 1;
    public float overXTime = 5;

    private void Start()
    {
        difficulty.SetValue(startingDifficulty);
    }
    public override void UpdateEventRaised(float deltaTime)
    {
        base.UpdateEventRaised(deltaTime);
        difficulty.SetValue(difficulty.GetValue() + ( deltaTime / overXTime * ((increasePercent / 100) * difficulty.GetValue()) ) );
    }
}
