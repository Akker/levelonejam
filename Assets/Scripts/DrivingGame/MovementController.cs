﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : OnUpdate 
{
	public Vector3 MovementForce = new Vector3(10f, 0f, 0f);
	public Vector3 SteeringForce = new Vector3(10f, 0f, 10f);
	public float acceleration = 25f;
	public float braking = 50f;
	public float steering = 4.0f;
	private Rigidbody Rigidbody;
	public bool InputActive = true;

	// Use this for initialization
	void Start () {
		Rigidbody = gameObject.GetComponent<Rigidbody>();
		// AkSoundEngine.PostEvent("EngineStart", gameObject);
		//AkSoundEngine.PostEvent("PlayTruckerFeedbackLoop", gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void UpdateEventRaised(float deltaTime)
	{
        Rigidbody.MovePosition(transform.position + (transform.right * Input.GetAxis("Horizontal") * Time.deltaTime * acceleration));
        return;
        if ( InputActive )
		{
			// Debug.Log(Input.GetAxis("Vertical"));
			// if( Input.GetAxis("Vertical") != 0 )
			// {
			// 	// gameObject.transform.Translate( transform.forward * Input.GetAxis("Vertical") * Time.deltaTime * acceleration, Space.World );
			// 	Rigidbody.AddForce( transform.forward * Input.GetAxis("Vertical") * acceleration );
			// }

			if( Input.GetAxis("Horizontal") != 0 )
			{
				//Debug.Log("Moving");
				//AkSoundEngine.PostEvent("Accelerate", gameObject);
				//gameObject.transform.Translate( transform.right * Input.GetAxis("Horizontal") * Time.deltaTime * acceleration, Space.Self );
                //Rigidbody.position = transform.position + (transform.right * Input.GetAxis("Horizontal") * Time.deltaTime * acceleration);
                Rigidbody.MovePosition(transform.position + (transform.right * Input.GetAxis("Horizontal") * Time.deltaTime * acceleration));
                // Rigidbody.AddForce( transform.right * Input.GetAxis("Horizontal") * acceleration );
                // Rigidbody.AddTorque( transform.up * Input.GetAxis("Horizontal") * steering );
            }

			// if( Input.GetButtonDown("Space") )
			// {
			// 	Rigidbody.AddForce(-braking * Rigidbody.velocity);
			// }
		}
	}
}
