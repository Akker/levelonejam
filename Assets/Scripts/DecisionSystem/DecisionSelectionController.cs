﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionSelectionController : MonoBehaviour
{
    public Decision[] possibleDecisions;
    public float[] chanceOfChoosing;

    public DecisionVariable upcomingDecision;
    public GameEvent firstDecisionUpcoming;
    private bool firstTime = true;

    private void Start()
    {
        
        chanceOfChoosing = new float[possibleDecisions.Length];
        float startingValue = 100/chanceOfChoosing.Length;
        for (int i = 0; i < chanceOfChoosing.Length; i++)
        {
            chanceOfChoosing[i] = startingValue;
        }
    }

    public void ChooseNextDecision()
    {
        float randomNumber = Random.Range(0, 100);
        float checkedNumber = 0.0f;
        for (int i = 0; i < chanceOfChoosing.Length; i++)
        {
            checkedNumber += chanceOfChoosing[i];
            if (checkedNumber > randomNumber)
            {
                ChooseDecision(i);
                return;
            }
        }
        ChooseDecision(0);
    }

    void ChooseDecision(int index)
    {
        Debug.Log("Choosing next decision");
        upcomingDecision.SetValue(possibleDecisions[index]);
        if( firstTime )
        {
            firstTime = false;
            firstDecisionUpcoming.Raise();  
        }

        float amountToShare = chanceOfChoosing[index] - 1;
        float amountToEach = amountToShare / (chanceOfChoosing.Length - 1);
        chanceOfChoosing[index] = 1;
        for (int i = 0; i < chanceOfChoosing.Length; i++)
        {
            if (i == index)
            {
                continue;
            }
            chanceOfChoosing[i] += amountToEach;
        }
    }

}
