﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecisionChoosingController : MonoBehaviour
{
    public DecisionVariable upcomingDecision;

    public IntVariable fucks;
    public IntVariable trucks;

    public GameEvent responseSoundFinished;
    public float maxWaitTime = 5;
    bool isWaiting = false;
    float currentTime = 0;
    object myCookieObject;

    private void Awake()
    {
        myCookieObject = new Object();
    }

    public void ChooseAction(int index)
    {
        fucks.ApplyChange(upcomingDecision.Value.fucks[index]);
        if (fucks.GetValue() >100)
        {
            fucks.SetValue(100);
        }
        trucks.ApplyChange(upcomingDecision.Value.truck[index]);
        if (upcomingDecision.Value.audio[index] != "")
        {
            isWaiting = true;
            currentTime = 0;
            AkSoundEngine.PostEvent(upcomingDecision.Value.audio[index], gameObject, (uint)AkCallbackType.AK_EndOfEvent, RaiseSoundFinishedEvent,myCookieObject);
        }
    }
    private void Update()
    {
        if (isWaiting)
        {
            currentTime += Time.deltaTime;
            if (currentTime > maxWaitTime)
            {
                isWaiting = false;
                responseSoundFinished.Raise();
            }
        }
    }

    public void RaiseSoundFinishedEvent(object in_cookie, AkCallbackType in_type, object in_info)
    {
        if (in_type == AkCallbackType.AK_EndOfEvent)
        {
            responseSoundFinished.Raise();
            isWaiting = false;
        }
    }
}
