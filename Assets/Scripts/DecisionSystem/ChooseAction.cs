﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChooseAction : MonoBehaviour, IPointerClickHandler
{
    bool acceptClicks = true;
    public GameEvent eventToRaise;
    public void OnPointerClick(PointerEventData eventData)
    {
        if (!acceptClicks) { return; }
        eventToRaise.Raise();
        acceptClicks = false;
    }

    void OnEnable()
    {
        acceptClicks = true;
    }
}
