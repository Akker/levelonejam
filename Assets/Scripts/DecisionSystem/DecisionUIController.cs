﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DecisionUIController : MonoBehaviour
{
    public DecisionVariable upcomingDecision;
    public Image background;
    public Text introductionText;
    public Text[] responseText;
    public GameEvent decisionOver;
    bool isVisible;
    bool soundFinished = true;


    int chosenDecisionNumber;
    int offset = 100;
    float moveTime = 2f;
    Vector2[] orgXPosition;
    Vector2[] offsetPositions;
    float[] visibility;
    private void Start()
    {
        orgXPosition = new Vector2[responseText.Length];
        offsetPositions = new Vector2[responseText.Length];
        visibility = new float[responseText.Length];

        for (int i = 0; i < orgXPosition.Length; i++)
        {
            orgXPosition[i] = responseText[i].GetComponent<RectTransform>().position;
            offsetPositions[i] = orgXPosition[i];
            offsetPositions[i].x +=offset;
        }
    }
    public void ShowDecision()
    {
        UpdateDecision();
        isVisible = true;
        background.gameObject.SetActive(true);
        if (upcomingDecision.Value.audio[0] != "")
        {
            soundFinished = false;
        }
    }
    public void HideDecision(int index)
    {
        chosenDecisionNumber = index;
        isVisible = false;
    }

    void UpdateDecision()
    {
        introductionText.text = upcomingDecision.Value.introduction;
        RectTransform tempRect;
        Color tempColor;
        for (int i = 0; i < upcomingDecision.Value.numberOfActions; i++)
        {
            responseText[i].text = upcomingDecision.Value.responses[i];
            //Color change
            tempColor = responseText[i].color;
            tempColor.a = 0.0f;
            responseText[i].color = tempColor;

            //Position Change
            tempRect = responseText[i].GetComponent<RectTransform>();
            tempRect.position = offsetPositions[i];

            visibility[i] = 0;
        }
    }

    
    private void Update()
    {
        if (isVisible)
        {
            SlowlyRevealDecision();
        }
        else
        {
            SlowlyRemoveDecision();
        }

    }

    public void ActionResponseFinished()
    {
        soundFinished = true;
    }

    void SlowlyRemoveDecision()
    {
        if (!background.gameObject.activeSelf) { return; }
        for (int i = upcomingDecision.Value.numberOfActions -1; i >= 0 ; i--)
        {
            if (i == chosenDecisionNumber)
            {
                continue;
            }
            visibility[i] -= Time.deltaTime * moveTime;
            //PositionChange
            RectTransform tempRect;
            tempRect = responseText[i].GetComponent<RectTransform>();

            tempRect.position = Vector2.Lerp(orgXPosition[i], offsetPositions[i], visibility[i]);
            

            //Color change
            Color tempColor = responseText[i].color;
            tempColor.a = visibility[i];
            responseText[i].color = tempColor;
            if (visibility[i] < 0.0f)
            {
                continue;
            }
            return;
        }

        if (visibility[chosenDecisionNumber] != 0.0f)
        {
            if (!soundFinished) { return; }
            visibility[chosenDecisionNumber] -= Time.deltaTime * moveTime;
            RectTransform tempRect;
            tempRect = responseText[chosenDecisionNumber].GetComponent<RectTransform>();

            tempRect.position = Vector2.Lerp(orgXPosition[chosenDecisionNumber], offsetPositions[chosenDecisionNumber], visibility[chosenDecisionNumber]);


            //Color change
            Color tempColor = responseText[chosenDecisionNumber].color;
            tempColor.a = visibility[chosenDecisionNumber];
            responseText[chosenDecisionNumber].color = tempColor;
            if (visibility[chosenDecisionNumber] < 0.0f)
            {
                visibility[chosenDecisionNumber] = 0.0f;
            }
            return;
        }

        if (introductionText.color.a != 0.0f)
        {
            Color tempColor = introductionText.color;
            tempColor.a -= Time.deltaTime;
            if (tempColor.a < 0.0f)
            {
                tempColor.a = 0.0f;
            }
            introductionText.color = tempColor;
            return;
        }
        if (background.color.a != 0.0f)
        {
            Color tempColor = background.color;
            tempColor.a -= Time.deltaTime;
            if (tempColor.a < 0.0f)
            {
                tempColor.a = 0.0f;
            }
            background.color = tempColor;
            return;
        }
        if (upcomingDecision.Value.audioOuttro != "")
        {
            AkSoundEngine.PostEvent(upcomingDecision.Value.audioOuttro,gameObject);
        }
        background.gameObject.SetActive(false);
        decisionOver.Raise();
    }

    void SlowlyRevealDecision()
    {
        if (background.color.a != 1.0f)
        {
            Color tempColor = background.color;
            tempColor.a += Time.deltaTime;
            if (tempColor.a > 1.0f)
            {
                tempColor.a = 1.0f;
            }
            background.color = tempColor;
            return;
        }

        if (introductionText.color.a != 1.0f)
        {
            Color tempColor = introductionText.color;
            tempColor.a += Time.deltaTime * moveTime;
            if (tempColor.a > 1.0f)
            {
                tempColor.a = 1.0f;
            }
            introductionText.color = tempColor;
            return;
        }

        for (int i = 0; i < upcomingDecision.Value.numberOfActions; i++)
        {
            visibility[i] += Time.deltaTime * moveTime;
            RectTransform tempRect;
            tempRect = responseText[i].GetComponent<RectTransform>();
            Color tempColor = responseText[i].color;
            if (visibility[i] >= 1.0f)
            {
                visibility[i] = 1.0f;
                tempColor.a += 1.0f;
                tempRect.position = Vector2.Lerp(orgXPosition[i], offsetPositions[i], 1.0f);
                responseText[i].color = tempColor;
                continue;
            }
            //PositionChange
            tempRect.position = Vector2.Lerp(orgXPosition[i], offsetPositions[i], visibility[i]);


            //Color change
            tempColor.a += visibility[i];
            responseText[i].color = tempColor;
            return;
        }
    }
}
