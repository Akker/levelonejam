﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System;

public class AlphaChanger : MonoBehaviour {

    public UnityEvent responseWhenDone;
	float step;
	float currentAlpha;
	Image image;
    private void OnEnable()
    {
        StartChange(1, 2.0f);
    }
    public void StartChange(int sign,float timeToReach)
	{
		image = GetComponent<Image>();
		step = Time.fixedDeltaTime /timeToReach;
		step = step * sign;
		//Debug.Log("Step: " +step);
		currentAlpha = image.color.a;
		InvokeRepeating("ChangeAlpha", Time.fixedDeltaTime,Time.fixedDeltaTime);
	}
	void ChangeAlpha()
	{
		Color newColor = image.color;
		currentAlpha += step;
		newColor.a = currentAlpha;
		image.color = newColor;
		if (currentAlpha < 0)
		{
			Stop();
		} else if (currentAlpha > 1)
		{
			Stop();
		}
	}
	void Stop()
	{
			CancelInvoke("ChangeAlpha");
			responseWhenDone.Invoke();
	}
}
