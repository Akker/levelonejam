﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Video;

public class MoviePlayer : MonoBehaviour {
	public Camera cam;
	public VideoClip clip;
	bool haveStarted = false;
	float currentTime = 0;
	public VideoPlayer videoPlayer;
	public UnityEvent response;
	public SceneSetVariable setVariable;
	public SceneSet menuScene;


	// Use this for initialization
	void Start () {
		//AkSoundEngine.PostEvent("PauseAll",gameObject);
		setVariable.SetValue(menuScene);
		StartCoroutine(playVideo());
	}


	IEnumerator playVideo()
	{
		//AkSoundEngine.PrepareEvent(AkPreparationType.Preparation_LoadAndDecode,new string[]{"PlayTrailerAudio"},1);
		//videoPlayer = cam.gameObject.AddComponent<UnityEngine.Video.VideoPlayer>();
		//var audioSource = cam.gameObject.AddComponent<AudioSource>();
		videoPlayer.playOnAwake = false;
		//audioSource.playOnAwake = false;
		
		videoPlayer.clip = clip;

		//audioSource.clip = clip.aui;
        // By default, VideoPlayers added to a camera will use the far plane.
        // Let's target the near plane instead.
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;

        // This will cause our scene to be visible through the video being played.

        // Set the video to play. URL supports local absolute or relative paths.
        // Here, using absolute.
        //videoPlayer.url = "/Users/graham/movie.mov";

        // Skip the first 100 frames.

        // Restart from beginning when done.
        videoPlayer.isLooping = false;
		videoPlayer.audioOutputMode = VideoAudioOutputMode.None;
		//videoPlayer.EnableAudioTrack(0,true);
		//videoPlayer.SetTargetAudioSource(0,audioSource);
        // Each time we reach the end, we slow down the playback by a factor of 10.
        //videoPlayer.loopPointReached += EndReached;
		videoPlayer.Prepare();
		while(!videoPlayer.isPrepared)
		{
			//Debug.Log("Preparing video");
			yield return null;
		}
			//Debug.Log("Done Preparing video");

        // Start playback. This means the VideoPlayer may have to prepare (reserve
        // resources, pre-load a few frames, etc.). To better control the delays
        // associated with this preparation one can use videoPlayer.Prepare() along with
        // its prepareCompleted event.
        
		//videoPlayer.Play();
		//AkSoundEngine.PostEvent("PlayTrailerAudio",gameObject);
		//audioSource.Play();

	}
	public void LoadMainMenu()
	{
		//AkSoundEngine.PostEvent("MenuMusic", gameObject);
		response.Invoke();
	}
	
	// Update is called once per frame
	void Update () {
		/* 
		return;
		if (!haveStarted)
		{
			if (videoPlayer.isPlaying)
			{
				haveStarted = true;
			AkSoundEngine.PostEvent("PlayTrailerAudio",gameObject);

			}
		}
		*/
		
		if (haveStarted) 
		{
			if (!videoPlayer.isPlaying)
			{
				LoadMainMenu();
			}
			return;
		}
		currentTime += Time.deltaTime;

		if (currentTime > 0.5f)
		{
			//StartCoroutine(playVideo());
			videoPlayer.Play();
			//AkSoundEngine.PostEvent("PlayTrailerAudio",gameObject);
			haveStarted = true;
		}
	}
}
