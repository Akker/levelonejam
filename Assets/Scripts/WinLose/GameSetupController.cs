﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetupController : MonoBehaviour
{
    [Header("StartValues")]
    public int fucksToGive;
    public int truckStatus;

    [Header("References")]
    public IntVariable fucks;
    public IntVariable trucks;

    private void Start()
    {
        fucks.SetValue(fucksToGive);
        trucks.SetValue(truckStatus);
    }
}
