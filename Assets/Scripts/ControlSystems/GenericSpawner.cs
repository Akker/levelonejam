﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GenericSpawner : MonoBehaviour
{
    public FloatReference difficulty;
    public GameObject[] objectsToSpawn;
    public float yOffset = 0;
    public float minSpeed = 5;
    public float maxSpeed = 10;
    public bool facingBackwards = false;
    Vector3 spawnPosition;

    List<GameObject>[] objectsSpawned;

    private void Start()
    {
        spawnPosition = transform.position;
        spawnPosition.y += yOffset;
        objectsSpawned = new List<GameObject>[objectsToSpawn.Length];
        for (int i = 0; i < objectsToSpawn.Length; i++)
        {
            objectsSpawned[i] = new List<GameObject>();
        }
    }

    public void Spawn()
    {
        int index = Random.Range(0, objectsToSpawn.Length);
        Spawn(index);
    }
    public void Spawn(int index)
    {
        GameObject tempObject = GetSpawnedObject(index);
        tempObject.transform.position = spawnPosition;
        tempObject.SetActive(true);
        if (facingBackwards)
        {
            tempObject.transform.forward = -transform.forward;
        }
        MovingObject tempMovingObject = tempObject.GetComponent<MovingObject>();
        tempMovingObject.Speed = Random.Range(minSpeed * difficulty, maxSpeed * difficulty);
        tempMovingObject.Direction = transform.forward;
    }

    GameObject GetSpawnedObject(int index)
    {
        for (int i = 0; i < objectsSpawned[index].Count; i++)
        {
            if (!objectsSpawned[index][i].activeSelf)
            {
                return objectsSpawned[index][i];
            }
        }
        objectsSpawned[index].Add(Instantiate(objectsToSpawn[index]));
        return objectsSpawned[index][objectsSpawned[index].Count - 1];
    }
}
