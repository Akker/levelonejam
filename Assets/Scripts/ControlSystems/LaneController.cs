﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaneController : OnUpdate
{
    public GenericSpawner[] spawners;
    public FloatReference difficulty;
    public float[] minTime;
    public float[] maxTime;
    float[] currentSpeed;
    float[] timeToReach;
    bool isSuspended;

    public void SuspendSpawning()
    {
        isSuspended = true;
    }
    public void ResumeSpawning()
    {
        isSuspended = false;
    }
    private void Start()
    {
        currentSpeed = new float[minTime.Length];
        timeToReach = new float[minTime.Length];
        for (int i = 0; i < currentSpeed.Length; i++)
        {
            timeToReach[i] = Random.Range(minTime[i] - ((minTime[i]* difficulty) - minTime[i]), maxTime[i] - ((maxTime[i] * difficulty) - maxTime[i]));
        }
    }

    public override void UpdateEventRaised(float deltaTime)
    {
        if (isSuspended) { return; }
        ProgressCurrentTime(deltaTime);
        CheckCurrentTime();
    }

    void ProgressCurrentTime(float deltaTime)
    {
        for (int i = 0; i < currentSpeed.Length; i++)
        {
            currentSpeed[i] += deltaTime;
        }
    }
    void CheckCurrentTime()
    {
        for (int i = 0; i < currentSpeed.Length; i++)
        {
            if (currentSpeed[i] > timeToReach[i])
            {
                currentSpeed[i] = 0;
                //timeToReach[i] = Random.Range(minTime[i], maxTime[i]);
                timeToReach[i] = Random.Range(minTime[i] - ((minTime[i] * difficulty) - minTime[i]), maxTime[i] - ((maxTime[i] * difficulty) - maxTime[i]));

                Spawn(i);
            }
        }
    }
    void Spawn(int index)
    {
        spawners[index].Spawn();
    }
}
