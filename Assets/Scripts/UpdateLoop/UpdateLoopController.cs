﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLoopController : MonoBehaviour {
	public UpdateEvent updateLoop;
	bool isRunning = false;

	
	void Update () {
		if (isRunning)
		{
			updateLoop.Raise(Time.deltaTime);
		}
	}
	public void StopUpdateLoop()
	{
		isRunning = false;
		//Debug.Log("UpdateLoop is stopped");
	}
	public void RunUpdateLoop()
	{
		isRunning = true;
		//Debug.Log("UpdateLoop is running");
	}
}
