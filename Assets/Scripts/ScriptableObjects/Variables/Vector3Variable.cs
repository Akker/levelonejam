﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[CreateAssetMenu(fileName ="Variables", menuName = "Variables/Vector3 Variable", order = 5)]
public class Vector3Variable : GameEvent {

	
    public Vector3 Value;

    public void SetValue(Vector3 value)
    {
        Value = value;
        Raise();

    }

    public void SetValue(Vector3Variable value)
    {
        Value = value.Value;
        Raise();

    }

    public void ApplyChange(Vector3 amount)
    {
        Value += amount;
        Raise();
    }

    public void ApplyChange(Vector3Variable amount)
    {
        Value += amount.Value;
        Raise();
    }
}
