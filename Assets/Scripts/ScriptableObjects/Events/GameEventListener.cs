﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class GameEventListener : MonoBehaviour {

     #if UNITY_EDITOR

    public string DeveloperDescription = "";
    #endif
    public GameEvent Event;
    public UnityEvent Response;
    public void OnEventRaised()
    {
        Response.Invoke();
    }
    private void OnEnable()
    {
        if (Event != null)
            Event.RegisterListener(this);
    }
    private void OnDisable()
    {
        if (Event != null)
            Event.UnRegisterListener(this);
    }
}

