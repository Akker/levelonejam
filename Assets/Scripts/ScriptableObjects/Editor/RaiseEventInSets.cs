﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(BooleanSet))]

public class RaiseEventInSets : Editor {

	 public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUI.enabled = Application.isPlaying;

        BooleanSet e = target as BooleanSet;
        if (GUILayout.Button("Raise"))
            e.Raise();
    }
}

