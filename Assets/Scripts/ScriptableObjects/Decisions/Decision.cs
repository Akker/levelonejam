﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Decision", menuName = "Decisions/Decision", order = 0)]
public class Decision : ScriptableObject
{

    
    [SerializeField]
    public Sprite sprite;
    [SerializeField]
    public string introduction;
    [SerializeField]
    public string audioIntroduction;
    [SerializeField]
    public string audioOuttro;

    public int numberOfActions = 3;
    [SerializeField]
    [HideInInspector]
    public string[] responses = new string[3];
    [SerializeField]
    [HideInInspector]

    public string[] audio = new string[3];
    [SerializeField]
    [HideInInspector]

    public int[] fucks = new int[3];
    [SerializeField]
    [HideInInspector]
    public int[] truck = new int[3];
    public void SetupAllArrays()
    {
        int prevSize = responses.Length;
        string[] tempresponses = responses;
        string[] tempaudio = audio;
        int[] tempconsequences = fucks;
        int[] tempTrunk = truck;

        responses = new string[numberOfActions];
        audio = new string[numberOfActions];
        fucks = new int[numberOfActions];
        truck = new int[numberOfActions];

        for (int i = 0; i < numberOfActions; i++)
        {
            if (i < prevSize)
            {
                responses[i] = tempresponses[i];
                audio[i] = tempaudio[i];
                fucks[i] = tempconsequences[i];
                truck[i] = tempTrunk[i];
            }else
            {
                responses[i] = "";
                audio[i] = "";
            }
        }
    }
}
