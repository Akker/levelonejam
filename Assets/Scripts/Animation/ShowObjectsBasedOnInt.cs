﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowObjectsBasedOnInt : GameEventListener
{
    [Header("IGNORE EVERYTHING ABOVE, Will sign up to event itself")]

    public IntVariable dependedInt;
    public GameObject[] images;
    public int[] changeToBelow;

    // Start is called before the first frame update
    void Start()
    {
        dependedInt.RegisterListener(this);
        Response.AddListener(VaribleChange);
        if (images.Length != changeToBelow.Length)
        {
            Debug.LogError("Difference in sprite and values, aborting");
            Destroy(this);
        }
        
    }

    public void VaribleChange()
    {
        Debug.Log("Change incoming");
        for (int i = 0; i < changeToBelow.Length; i++)
        {
            if (dependedInt.GetValue() < changeToBelow[i])
            {
                images[i].SetActive(true);
            } else
            {
                images[i].SetActive(false);

            }
        }
    }
}
