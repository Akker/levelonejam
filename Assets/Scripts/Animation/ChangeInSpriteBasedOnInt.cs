﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeInSpriteBasedOnInt : MonoBehaviour
{
    public IntVariable dependedInt;
    public Sprite[] sprites;
    public int[] changeToBelow;
    public Image imageToChange;
    public bool isAbove;
    // Start is called before the first frame update
    void Start()
    {
        
        if (sprites.Length != changeToBelow.Length)
        {
            Debug.LogError("Difference in sprite and values, aborting");
            Destroy(this);
        }
    }

    public void VaribleChange()
    {
        if (isAbove)
        {
            Beabove();
        } else
        {
            BeBelow();
        }
    }
    void Beabove()
    {
        //Debug.Log(dependedInt.GetValue());

        for (int i = changeToBelow.Length - 1; i >= 0; i--)
        {

            if (dependedInt.GetValue() > changeToBelow[i])
            {
                imageToChange.sprite = sprites[i];
                return;
            }
        }
    }
    void BeBelow()
    {
        for (int i = 0; i < changeToBelow.Length; i++)
        {

            if (dependedInt.GetValue() <= changeToBelow[i])
            {
                imageToChange.sprite = sprites[i];
                return;
            }
        }
    }
}
