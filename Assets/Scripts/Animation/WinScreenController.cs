﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinScreenController : MonoBehaviour
{
    public Text introductionText;
    public Text action;
    public float offset = 100;
    public float moveTime = 2.0f;
    Vector3 orgPosition;
    Vector3 offsetPosition;
    float visibility = 0;
    Color invisible = new Color(1.0f, 1.0f, 1.0f, 0.0f);

    float timeToReach = 1.0f;
    float currentTime;
    // Start is called before the first frame update
    void Start()
    {
        Vector3 newPosition = action.transform.position;
        orgPosition = newPosition;
        newPosition.x += offset;
        offsetPosition = newPosition;
        action.transform.position = newPosition;

        action.color = invisible;
        introductionText.color = invisible;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if(timeToReach> currentTime) { return; }
        Color tempColor;
        if (introductionText.color.a != 1.0f)
        {
            tempColor = introductionText.color;
            tempColor.a += Time.deltaTime * moveTime;
            if (tempColor.a > 1.0f)
            {
                tempColor.a = 1.0f;
            }
            introductionText.color = tempColor;
            return;
        }

        visibility += Time.deltaTime * moveTime;
        RectTransform tempRect;
        tempRect = action.GetComponent<RectTransform>();
        tempColor = action.color;
        tempColor.a = visibility;
        tempRect.position = Vector2.Lerp(orgPosition, offsetPosition, visibility);
        action.color = tempColor;

        if (visibility >= 1.0f)
        {
            visibility = 1.0f;
            tempColor.a = 1.0f;
            tempRect.position = Vector2.Lerp(orgPosition, offsetPosition, 1.0f);
            action.color = tempColor;
            Destroy(this);
        }
    }
}
