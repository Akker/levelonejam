﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAdd2DForce : OnUpdate
{
    public Rigidbody2D rb2D;
    public float minForce;
    public float maxForce;

    public float minTime;
    public float maxTime;

    float currentTime;
    float timeToReach;

    private void Start()
    {
        FindNewTime();
    }
    void FindNewTime()
    {
        timeToReach = Random.Range(minTime, maxTime);
        currentTime = 0;
    }

    public override void UpdateEventRaised(float deltaTime)
    {
        base.UpdateEventRaised(deltaTime);
        currentTime += deltaTime;
        if (currentTime > timeToReach)
        {
            AddForce();
            FindNewTime();
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            AddForce();
        }
    }
    void AddForce()
    {
        //new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f))
        rb2D.AddForce(new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)) * Random.Range(minForce, maxForce),ForceMode2D.Impulse);
    }
}
