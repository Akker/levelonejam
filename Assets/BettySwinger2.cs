﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BettySwinger2 : OnUpdate
{
    public float delta = 1.5f;  // Amount to move left and right from the start point
    public float speed = 2.0f;
    public float direction = 1;
    private Quaternion startPos;

    private void Awake() 
    {
        startPos = transform.rotation;      
    }
    private void Start()
    {
        // startPosition = transform.position;
    }

    public override void UpdateEventRaised(float deltaTime)
    {
            Quaternion a = startPos;
            // direction = Input.GetAxis("Horizontal") > 0 ? 1 : -1;
            a.z += (deltaTime * Input.GetAxis("Horizontal") * speed);
            transform.rotation = a;       
    }
}
